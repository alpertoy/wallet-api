![](image/diagram.png)

<h4>Before testing on Swagger UI, get JWT via endpoint below or simply use Postman test collection</h4>

``
http://localhost:8080/auth
``

or use via Postman

``
https://wallet-api-alper.herokuapp.com/auth
``

<h4>Swagger UI</h4>

``
http://localhost:8080/swagger-ui/index.html
``

or live on

``
https://wallet-api-alper.herokuapp.com/swagger-ui/index.html
``

<h4>Postman Test Collection</h4>

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/393f73691b5ee5a6a19d?action=collection%2Fimport)

