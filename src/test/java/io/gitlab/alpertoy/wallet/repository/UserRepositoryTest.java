package io.gitlab.alpertoy.wallet.repository;

import io.gitlab.alpertoy.wallet.entity.User;
import io.gitlab.alpertoy.wallet.util.enums.RoleEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserRepositoryTest {

    private static final String EMAIL = "email@test.com";
    @Autowired
    UserRepository repository;

    @BeforeEach
    public void setUp() {
        User u = new User();
        u.setName("Set up User");
        u.setPassword("Password123");
        u.setEmail(EMAIL);
        u.setRole(RoleEnum.ROLE_ADMIN);

        repository.save(u);
    }

    @AfterEach
    public void tearDown() {
        repository.deleteAll();
    }

    @Test
    public void testSave() {
        User u = new User();
        u.setName("Test");
        u.setPassword("12345");
        u.setEmail("test@test.com");
        u.setRole(RoleEnum.ROLE_ADMIN);

        User response = repository.save(u);

        assertNotNull(response);
    }

    @Test
    public void testFindByEmail() {
        Optional<User> response = repository.findByEmail(EMAIL);

        assertTrue(response.isPresent());
        assertEquals(response.get().getEmail(), EMAIL);
    }
}
