package io.gitlab.alpertoy.wallet.repository;

import io.gitlab.alpertoy.wallet.entity.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletRepository extends JpaRepository<Wallet, Long> {
}
