package io.gitlab.alpertoy.wallet.util.enums;

public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER;
}
