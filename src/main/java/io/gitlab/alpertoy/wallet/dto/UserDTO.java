package io.gitlab.alpertoy.wallet.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    private Long id;
    @Email(message = "Invalid email")
    private String email;
    @Length(min = 3, max = 50, message = "The name must contain between 3 and 50 characters")
    private String name;
    @NotNull
    @Length(min = 6, message = "The password must contain at least 6 characters")
    private String password;
    @NotNull(message = "Enter a role")
    @Pattern(regexp = "^(ROLE_ADMIN|ROLE_USER)$", message = "For the role, only ROLE_ADMIN or ROLE_USER values are accepted")
    private String role;
}
