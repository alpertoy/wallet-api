package io.gitlab.alpertoy.wallet.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserWalletDTO {

    private Long id;
    @NotNull(message = "Enter user id")
    private Long users;
    @NotNull(message = "Enter wallet id")
    private Long wallet;
}
