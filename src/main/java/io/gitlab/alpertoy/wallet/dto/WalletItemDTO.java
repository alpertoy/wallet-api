package io.gitlab.alpertoy.wallet.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class WalletItemDTO {

    private Long id;
    @NotNull(message = "Enter wallet id")
    private Long wallet;
    @NotNull(message = "Enter a date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", locale = "tr-TR", timezone = "Europe/Istanbul")
    private Date date;
    @NotNull(message = "Enter a type")
    @Pattern(regexp = "^(INPUT|OUTPUT)$", message = "For the type, only INPUT or OUTPUT values are accepted")
    private String type;
    @NotNull(message = "Enter a description")
    @Length(min = 5, message = "Description must be at least 5 characters")
    private String description;
    @NotNull(message = "Enter a value")
    private BigDecimal value;

}
