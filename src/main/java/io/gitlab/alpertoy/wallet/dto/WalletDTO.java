package io.gitlab.alpertoy.wallet.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class WalletDTO {

    private Long id;
    @Length(min = 3, message = "Name must contain at least 3 characters")
    @NotNull(message = "Name cannot be null")
    private String name;
    @NotNull(message = "Enter a value for the wallet")
    private BigDecimal value;
}
