package io.gitlab.alpertoy.wallet.security.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class JwtAuthenticationDTO {

    @NotNull(message = "Enter an email")
    @NotBlank(message = "Enter an email")
    private String email;
    @NotNull(message = "Enter a password")
    @NotBlank(message = "Enter a password")
    private String password;

}
