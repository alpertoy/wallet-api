package io.gitlab.alpertoy.wallet.service;

import io.gitlab.alpertoy.wallet.entity.UserWallet;

import java.util.Optional;

public interface UserWalletService {

    UserWallet save(UserWallet uw);

    Optional<UserWallet> findByUsersIdAndWalletId(Long user, Long wallet);
}
