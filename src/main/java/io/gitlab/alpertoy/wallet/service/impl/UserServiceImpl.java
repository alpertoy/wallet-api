package io.gitlab.alpertoy.wallet.service.impl;

import io.gitlab.alpertoy.wallet.entity.User;
import io.gitlab.alpertoy.wallet.repository.UserRepository;
import io.gitlab.alpertoy.wallet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repository;

    @Override
    public User save(User u) {
        return repository.save(u);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return repository.findByEmail(email);
    }
}
