package io.gitlab.alpertoy.wallet.service;

import io.gitlab.alpertoy.wallet.entity.Wallet;

public interface WalletService {

    Wallet save(Wallet w);
}
