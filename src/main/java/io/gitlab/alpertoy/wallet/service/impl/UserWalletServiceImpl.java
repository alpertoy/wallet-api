package io.gitlab.alpertoy.wallet.service.impl;

import io.gitlab.alpertoy.wallet.entity.UserWallet;
import io.gitlab.alpertoy.wallet.repository.UserWalletRepository;
import io.gitlab.alpertoy.wallet.service.UserWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserWalletServiceImpl implements UserWalletService {

    @Autowired
    UserWalletRepository repository;

    @Override
    public UserWallet save(UserWallet uw) {
        return repository.save(uw);
    }

    @Override
    public Optional<UserWallet> findByUsersIdAndWalletId(Long user, Long wallet) {
        return repository.findByUsersIdAndWalletId(user, wallet);
    }
}
