package io.gitlab.alpertoy.wallet.service;

import io.gitlab.alpertoy.wallet.entity.User;

import java.util.Optional;

public interface UserService {

    User save(User u);

    Optional<User> findByEmail(String email);
}
